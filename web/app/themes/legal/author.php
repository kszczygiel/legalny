<?
$context = Timber::get_context();

$tmp_author = $wp_query->get_queried_object();

$curr_author = [
  'first_name' => $tmp_author->first_name
];

$context['author'] = $tmp_author;

$paged = get_query_var('paged') ? get_query_var('paged') : 1;
$args = [
  'post_type', array('post', 'page'),
  'author' =>  $context['author']->ID,
  'post__not_in' => array(3800,4430,3597,4096,6928,6777,3882,6016,4200,4137,3823),

];

$posts_wp = new WP_Query( $args );
$context['wp_pagenavi'] = wp_pagenavi(
  [
      'echo' => false,
      'query' => $posts_wp,
  ]
);
$all_posts = Timber::get_posts($args);
$context['chunks'] = array_chunk($all_posts, 2);
Timber::render('views/templates/author.twig', $context);
