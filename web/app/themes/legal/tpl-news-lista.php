<?php
/*
Template Name: Aktualności lista
*/

$paged = get_query_var('paged') ? get_query_var('paged') : 1;

if ( $paged > 1 ) {
	add_filter('wpseo_title', 'add_to_page_title', 100);
}

function add_to_page_title($title) {
	$paged = get_query_var('paged') ? get_query_var('paged') : 1;
	$title .= ' | Page ' . $paged;
	return $title;
}

$args = [
  'post_type' => 'post',
  'paged' => $paged,
  'posts_per_page' => 12,
  'cat' => 283
];

$posts_wp = new WP_Query( $args );


$context = Timber::get_context();

$main = [
    'numberposts'	=> 1,
    'post_type'		=> 'post',
    'meta_key'		=> 'wyroznione',
    'meta_value'	=> 1,
    'category' => array('283')
];

$context['main'] = Timber::get_posts($main)[0];

$context['wp_pagenavi'] = wp_pagenavi(
  [
      'echo' => false,
      'query' => $posts_wp,
  ]
);

$context['typy'] = Timber::get_posts($args);

$context['post'] = Timber::get_post();


foreach (get_field('powiazane_tagi') as $tag){
  $tag['tag'] = get_tag($tag['tag'][0]);

  $args = [
    'post_type' => 'post',
    'tag' => $tag['tag']->slug
  ];

  $context['powiazane_tagi'][] = $tag;
}

foreach($context['options']['kafelki_bukmacherzy'] as $single){
  $context['kafelki_bukmacherzy'][] = Timber::get_post($single['bukmacher']->ID);
}


Timber::render('views/templates/aktualnosci-lista.twig', $context);
