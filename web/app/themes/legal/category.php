<?php
$paged = get_query_var('paged') ? get_query_var('paged') : 1;

$args = [
  'post_type' => 'post',
  'paged' => $paged,
  'suppress_filters' => false,
  'cat'=> get_queried_object()->term_id
];

if ( $paged > 1 ) {
	add_filter('wpseo_title', 'add_to_page_title', 100);
}

function add_to_page_title($title) {
	$paged = get_query_var('paged') ? get_query_var('paged') : 1;
	$title .= ' | Page ' . $paged;
	return $title;
}

$posts_wp = new WP_Query( $args );

$context = Timber::get_context();

$context['tag'] = get_queried_object();


$context['wp_pagenavi'] = wp_pagenavi(
  [
      'echo' => false,
      'query' => $posts_wp,
  ]
);

$all_posts = Timber::get_posts($args);

$context['posts'] = $all_posts;
Timber::render('views/templates/typy-lista-simple.twig', $context);



