<?php
/**
 * Conditional tags for Timber
 */
function add_to_context( $data ) {
	$data['is_home']          = is_home();
	$data['is_front_page']    = is_front_page();
	$data['is_single']        = is_single();
	$data['is_page']          = is_page();
	$data['is_page_template'] = is_page_template();
	$data['is_category']      = is_category();
	$data['is_tag']           = is_tag();
	$data['is_tax']           = is_tax();
	$data['is_author']        = is_author();
	$data['is_date']          = is_date();
	$data['is_year']          = is_year();
	$data['is_month']         = is_month();
	$data['is_day']           = is_day();
	$data['is_archive']       = is_archive();
	$data['is_404']           = is_404();
	$data['is_paged']         = is_paged();
	$data['is_singular']      = is_singular();
	$data['is_main_query']    = is_main_query();
	$data['options'] = get_fields('options');
	$data['ukryj_dodatkowy'] = $data['options']['ukryj_dodatkowy'];
	$data['main_menu'] = new TimberMenu('main-menu');
	$data['footer1_menu'] = new TimberMenu('footer1');
	$data['footer2_menu'] = new TimberMenu('footer2');
	$data['footer3_menu'] = new TimberMenu('footer3');
	$data['search_bar'] = get_search_form(false);;
	$data['blog_url'] = get_permalink(get_option( 'page_for_posts' ));
	$data['all_recipes'] = get_post_type_archive_link('recipe');
	global $post;
	$data['bukmacher_sidebar'] = Timber::get_post(get_field('bukmacher_sidebar',$post->post_id));
	$data['first_month_day'] = new DateTime('first day of this month');


	$data['breadcrumbs'] = [];
	$data['breadcrumbs'][] = [
		'link' => '/',
		'title' => 'Home'
	];

	if(get_post_type($post->post_id) == 'bookmaker'){
		$data['breadcrumbs'][] = [
			'link' => get_the_permalink(4582),
			'title' => 'Bukmacherzy - Opinie'
		];

		$data['bukmacher_sidebar'] = Timber::get_post($post->ID);

	}

	$cats = [];


	foreach(get_the_category($post) as $cat ){
		$cats[] = $cat->term_id;
	}
	
//poradniki
	if( in_array('112', $cats)){
		$data['breadcrumbs'][] = [
			'link' => get_the_permalink(7397),
			'title' => 'Poradniki'
		];

		$tag = get_the_tags($post->post_id)[0];

		$data['breadcrumbs'][] = [
			'link' => get_tag_link($tag->term_id),
			'title' => $tag->name
		];
	}

	//typy

	if( in_array('49', $cats)){
		$data['breadcrumbs'][] = [
			'link' => get_the_permalink(7395),
			'title' => 'Typy'
		];

		$tag = get_queried_object();

		$data['breadcrumbs'][] = [
			'link' => get_tag_link($tag->term_id),
			'title' => $tag->name
		];
	}

	//newsy

	if( in_array('283', $cats)){
		$data['breadcrumbs'][] = [
			'link' => get_the_permalink(2267),
			'title' => 'News'
		];

		$tag = get_the_tags($post->post_id)[0];

		$data['breadcrumbs'][] = [
			'link' => get_tag_link($tag->term_id),
			'title' => $tag->name
		];
	}

	if(!is_archive()){

		$data['breadcrumbs'][] = [
			'link' => 'no-link-no',
			'title' => (get_field('breadcrumb_tekst', $post->ID) ? : $post->post_title)
		];
	}


	if(get_field('wersja_sidebara', $post->ID) == 'home' ){
		$data['home_sidebar'] = Timber::get_post(2);
		$data['bukmacher_sidebar'] = Timber::get_post(get_field('bukmacher_sidebar',2));
	}

	if(get_field('wersja_sidebara', $post->ID) == 'ranking' ){
	}
	
	$related = array();
	$tmp = get_field('powiazane_wpisy', $post->post_id);
	if(is_array($tmp)){
		foreach ($tmp as $single){
			$related[] = Timber::get_post($single);
		}
	}
	$data['related'] = $related;


	$term = get_queried_object();
	$data['header_h2'] = ( is_category() ? get_field('header_naglowek_h2', $term) : get_field('header_naglowek_h2') );
	$data['header_h2'] = ( $data['header_h2'] ? : $data['options']['h2_header']  );
	$data['header_h3'] = ( is_category() ? get_field('header_naglowek_h3', $term) : get_field('header_naglowek_h3') );
	$data['header_h3'] = ( $data['header_h3'] ? : $data['options']['h3_header']  );

	return $data;
}
add_filter('timber_context', 'add_to_context');

/**
 * Config vars
 */
function add_to_config( $data ) {
	/**
	 * Example
	 * $data['config']['name'] = 'value'l
	 */
	return $data;
}

add_filter('timber_context', 'add_to_config');
