<?php

$paged = get_query_var('paged') ? get_query_var('paged') : 1;

if ( $paged > 1 ) {
	add_filter('wpseo_title', 'add_to_page_title', 100);
}

function add_to_page_title($title) {
	$paged = get_query_var('paged') ? get_query_var('paged') : 1;
	$title .= ' | Page ' . $paged;
	return $title;
}

$args = [
  'post_type' => 'post',
  'paged' => $paged,
  'suppress_filters' => false,
  'tag'=> get_queried_object()->slug,
  'posts_per_page' => 12
];


$posts_wp = new WP_Query( $args );

$context = Timber::get_context();


$context['tag'] = get_queried_object();


$context['wp_pagenavi'] = wp_pagenavi(
  [
      'echo' => false,
      'query' => $posts_wp,
  ]
);

$all_posts = Timber::get_posts($args);

$cats = [];

foreach(get_the_category($all_posts[0]) as $cat ){
  $cats[] = $cat->term_id;
}

if( in_array('49', $cats)){
  $context['posts'] = $all_posts;
  Timber::render('views/templates/typy-lista-simple.twig', $context);
}

elseif( in_array('112', $cats) || in_array('283', $cats)){
  $context['chunks'] = array_chunk($all_posts, 2);
  Timber::render('views/templates/poradnik-lista-simple.twig', $context);
}

else{
  $context['posts'] = $all_posts;
  Timber::render('views/templates/typy-lista-simple.twig', $context);
}


