<?php
/*
Template Name: Ranking bukmacherów
*/

$context = Timber::get_context();
$context['post'] = Timber::get_post();
$args = [
  'numberposts' => 2,
  'category' => array('283')
];

$context['recent'] = Timber::get_posts($args);
$context['recent_link'] = get_category_link('283');

foreach($context['options']['lista_bukmacherow'] as $single){
  $context['lista_bukmacherow'][] = Timber::get_post($single['bukmacher']->ID);
}

foreach($context['options']['kafelki_bukmacherzy'] as $single){
  $context['kafelki_bukmacherzy'][] = Timber::get_post($single['bukmacher']->ID);
}

$context['context_list'] = list_context_headers_for_menu($context['post']->ID);


Timber::render('views/templates/ranking.twig', $context);
