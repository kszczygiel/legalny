<?php
/*
Template Name: Kod
Template Post Type: post, page
*/

$context = Timber::get_context();
$context['post'] = Timber::get_post();
$args = [
  'numberposts' => 2,
  'category' => array('283')
];

$context['recent'] = Timber::get_posts($args);
$context['recent_link'] = get_category_link('283');
$context['context_list'] = list_context_headers_for_menu($context['post']->ID);


$args = [
  'posts_per_page' => -1,
  'post_type' => 'review',
  'post_status' => 'publish',
  'numberposts' => -1,
  'meta_key' => 'related_post_id',
  'meta_value' => $context['post']->ID
];
$context['reviews'] = Timber::get_posts($args);


$context['bookmaker'] = Timber::get_post($context['post']->powiazany_bukmacher);

$context['hide_review_form'] = !in_array($context['post']->ID,explode(',',$_COOKIE['BlockReview']));

Timber::render('views/templates/kod.twig', $context);
