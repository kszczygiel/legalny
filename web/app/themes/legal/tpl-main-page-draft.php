<?php
/*
Template Name: Home Draft
*/

$context = Timber::get_context();
$context['post'] = Timber::get_post();
$args = [
  'numberposts' => 3,
  'category' => array('283')
];

$context['recent'] = Timber::get_posts($args);
$context['recent_link'] = get_category_link('283');

$args = [
  'numberposts' => 3,
  'category' => array('49')
];

$context['typy'] = Timber::get_posts($args);
$context['typy_link'] = get_category_link('49');

$args = [
  'numberposts' => 2,
  'category' => array('112'),
  'meta_key'		=> 'wyroznione',
  'meta_value'	=> '0',
];

$context['poradniki'] = Timber::get_posts($args);


$args = [
	'numberposts'	=> 1,
	'post_type'		=> 'post',
	'meta_key'		=> 'wyroznione',
  'meta_value'	=> '1',
  'category' => array('112')
];

$context['poradnik_main'] = Timber::get_posts($args);

$context['poradniki_link'] = get_category_link('112');


foreach($context['options']['kafelki_bukmacherzy'] as $single){
  $context['kafelki_bukmacherzy'][] = Timber::get_post($single['bukmacher']->ID);
}

foreach($context['options']['lista_bukmacherow'] as $single){
  $context['lista_bukmacherow'][] = Timber::get_post($single['bukmacher']->ID);
}

Timber::render('views/templates/main-page-draft.twig', $context);
