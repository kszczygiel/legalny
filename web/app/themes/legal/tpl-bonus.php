<?php
/*
Template Name: Bonus
Template Post Type: post, page
*/

$context = Timber::get_context();
$context['post'] = Timber::get_post();

$context['headings_list'] = get_heading_list($context['post']->ID);


Timber::render('views/templates/bonus.twig', $context);
