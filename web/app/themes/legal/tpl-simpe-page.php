<?php
/*
Template Name: Prosta tekstowa
Template Post Type: post, page
*/

$context = Timber::get_context();
$context['post'] = Timber::get_post();

Timber::render('views/templates/simple-text.twig', $context);
