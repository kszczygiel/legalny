<?php
/*
Template Name: News single
Template Post Type: post, page
*/

$context = Timber::get_context();
$context['post'] = Timber::get_post();

$context['headings_list'] = get_heading_list($context['post']->ID);

if(count($context['related']) < 3 ){
  $args = [
    'numberposts' => 3,
    'category' => array('283'),
    'post__not_in'     => array($context['post']->ID)
  ];
  
  $context['related'] = Timber::get_posts($args);
} 

foreach($context['options']['kafelki_bukmacherzy'] as $single){
  $context['kafelki_bukmacherzy'][] = Timber::get_post($single['bukmacher']->ID);
}


Timber::render('views/templates/news-single.twig', $context);
