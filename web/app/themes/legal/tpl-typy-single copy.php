<?php
/*
Template Name: Typy single
Template Post Type: post, page
*/

$context = Timber::get_context();
$context['post'] = Timber::get_post();
$args = [
  'numberposts' => 2,
  'category' => array('283')
];

$args = [
  'numberposts' => 2,
  'category' => array('49')
];

$context['typy'] = Timber::get_posts($args);
$context['typy_link'] = get_category_link('49');

$context['recent'] = Timber::get_posts($args);
$context['recent_link'] = get_category_link('283');

Timber::render('views/templates/typy-single.twig', $context);
