<?php
/*
Template Name: Typy lista
*/
wp_reset_query();
$paged = get_query_var('paged') ? get_query_var('paged') : 1;

if ( $paged > 1 ) {
	add_filter('wpseo_title', 'add_to_page_title', 100);
}

function add_to_page_title($title) {
	$paged = get_query_var('paged') ? get_query_var('paged') : 1;
	$title .= ' | Page ' . $paged;
	return $title;
}

$args = [
  'post_type' => 'post',
  'paged' => $paged,
  'posts_per_page' => 9,
  'cat' => 49
];

$posts_wp = new WP_Query('cat=49&showposts=9&paged='.$paged);

$context = Timber::get_context();

$context['show_description'] = false;

if( $paged == 1 ){
  $context['show_description'] = true;
}

$context['wp_pagenavi'] = wp_pagenavi(
  [
      'echo' => false,
      'query' => $posts_wp,
  ]
);

$context['typy'] = Timber::get_posts($args);
$context['post'] = Timber::get_post();


foreach (get_field('powiazane_tagi') as $tag){
  $tag['tag'] = get_tag($tag['tag'][0]);

  $args = [
    'post_type' => 'post',
    'tag' => $tag['tag']->slug
  ];

  $context['powiazane_tagi'][] = $tag;
}


Timber::render('views/templates/typy-lista.twig', $context);
