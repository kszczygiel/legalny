<?php
require_once __DIR__ . '/vendor/autoload.php';

$timber = new \Timber\Timber();

$sage_includes = [
	'lib/timber.php',
	'lib/assets.php',
	'lib/setup.php',
];

foreach ( $sage_includes as $file ) {
	$filepath = locate_template($file);
	if ( ! $filepath ) {
		trigger_error(sprintf(__('Error locating %s for inclusion', 'sasquatch'), $file), E_USER_ERROR);
	}

	require_once $filepath;
}

// Register Widgets
function custom_sidebar() {
$args = array(
'id' => 'under-single-post-content',
'name' => __( 'Single Post under content', 'text_domain' ),
'description' => __( 'Displays under content on single', 'text_domain' ),
'before_title' => '<h3 class="widget-title">',
'after_title' => '</h3>',
'before_widget' => '<section id="%1$s" class="widget %2$s">',
'after_widget' => '</section>',
);
register_sidebar( $args );

}
add_action( 'widgets_init', 'custom_sidebar' );

add_filter( 'get_the_archive_title', function ($title) {
    if ( is_category() ) {
            $title = single_cat_title( '', false );
        } elseif ( is_tag() ) {
            $title = single_tag_title( '', false );
        } elseif ( is_author() ) {
            $title = '<span class="vcard">' . get_the_author() . '</span>' ;
        }
    return $title;
});



// CSG FUNCTIONS
    // CSG register JS and css
add_action('wp_head', 'myplugin_ajaxurl');

    function myplugin_ajaxurl() {

        echo '<script type="text/javascript">
                var ajaxurl = "' . admin_url('admin-ajax.php') . '";
              </script><link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">';
     }


function csgCustom_enqueue_scripts() {
//	wp_enqueue_script( 'csg-custom', get_template_directory_uri() . '/csg-custom.js', '1.0.0' );
	wp_enqueue_script( 'csg-noty', get_template_directory_uri() . '/assets/js/lib/noty.min.js', '1.0.0' );
//	wp_enqueue_script( 'csg-slick', get_template_directory_uri() . '/slick.min.js', '1.0.0' );
}

// add_filter('wpseo_title', 'filter_custom_wpseo_title');
// function filter_custom_wpseo_title($title) {
//     global $post;
//     $c_title = get_field('tytul_seo_nadpis', $post->ID);
//     if($c_title){
//         $title = $c_title;
//     }
//     else{
//         $title = get_post_meta($post->ID, '_yoast_wpseo_title', true);
//     }

//     if(!$title){
//         $title = get_the_title($post->ID);
//     }

//     return $title;
// }

add_action( 'wp_enqueue_scripts', 'csgCustom_enqueue_scripts' );
add_action('wp_ajax_ajax_save_review', 'ajax_save_csg_review');
add_action('wp_ajax_nopriv_ajax_save_review', 'ajax_save_csg_review');
add_action('init', 'reviews_post_type');
add_action('init', 'bookmakers_post_type');
// add_action('init', 'promocodes_post_type');
add_filter( 'post_type_link', 'na_remove_slug', 10, 3 );
add_action( 'pre_get_posts', 'na_parse_request' );
add_action('init', 'flush_rewrite_rules');
add_action('init', 'redirect_old_sport_types');
add_action('wp_footer', 'search_for_faq_block');
add_action('wp_footer', 'safari_wp_image_fix');
add_action('wp_head', 'fb_pixel_code');
add_filter( 'the_content', 'auto_id_headings' );
add_action( 'manage_review_posts_custom_column', 'smashing_realestate_column', 10, 2);

add_action('acf/init', 'bet_bar_block_init');

function bet_bar_render_view( $block ) {
	
	// convert name ("acf/testimonial") into path friendly slug ("testimonial")
	$slug = str_replace('acf/', '', $block['name']);
	
	// include a template part from within the "template-parts/block" folder
	if( file_exists( get_theme_file_path("/blocks/block-kursy-belka.php") ) ) {
		include( get_theme_file_path("/blocks/block-kursy-belka.php") );
	}
}
function bet_bar_block_init() {
	
	// check function exists
	if( function_exists('acf_register_block') ) {
		
		// register a testimonial block
		acf_register_block(array(
			'name'				=> 'belka-kursy',
			'title'				=> __('belka kursy'),
			'description'		=> __('belka kursy'),
			'render_callback'	=> 'bet_bar_render_view',
			'category'			=> 'formatting',
			'icon'				=> 'admin-comments',
			'keywords'			=> array( 'belka-kursy', 'quote' ),
		));
	}
}

function smashing_realestate_column( $column, $post_id ) {
  // Image column
  if ( 'related' === $column ) {
    echo get_the_title(get_field( 'related_post_id', $post_id));
  }
}

add_filter( 'manage_review_posts_columns', 'smashing_filter_posts_columns' );
function smashing_filter_posts_columns( $columns ) {
  $columns['related'] = __( 'Related post' );
  return $columns;
}

function safari_wp_image_fix(){
    $user_agent = $_SERVER['HTTP_USER_AGENT']; 

    if (stripos( $user_agent, 'Safari') !== false){
        echo "<style>body .sidebar-content .content .wp-block-image{display:block!important;}</style>";
    }
}
function fb_pixel_code(){
    ?>
    <!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
 fbq('init', '185784549401878'); 
fbq('track', 'PageView');
</script>
<noscript>
 <img height="1" width="1" 
src="https://www.facebook.com/tr?id=185784549401878&ev=PageView
&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->
    <?php
}

function generate_li_element_sitemap($element){
    $curr_link = get_the_permalink($element->ID);
    if(!in_array($curr_link, EXCLUDED_FROM_CSV)){
        $html = '<li class="nav-drop-item">';
        $html .=  '<a href="'.$curr_link.'"><span>' . $element->post_title ;
        $html .= '</span></a></li>';
        echo $html;
    }
}

function generate_array_excluded_from_csv(){
    $row = 1;
    $csv_to_exclude = array();
    if (($handle = fopen(get_template_directory_uri()."/excluded.csv", "r")) !== FALSE) {
        while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
            $csv_to_exclude[] = $data[0]; 
            $row++;
            for ($c=0; $c < $num; $c++) {
                echo $data[$c] . "<br />\n";
            }
        }
        fclose($handle);
    }

    define("EXCLUDED_FROM_CSV",$csv_to_exclude);
}

function get_kod_link_by_recenzja_id($id){
    $posts = get_posts(array(
        'numberposts'	=> -1,
        'post_type'		=> 'post',
        'meta_key'		=> 'powiazany_bukmacher',
        'meta_value'	=> $id
    ));

    return get_permalink($posts[0]);

}

function filter_wpseo_robots( $robotsstr ) {

    if(is_tag() || is_author()){
        return 'noindex, follow';
    }
    return 'index, follow';
};

// add the filter
add_filter( 'wpseo_robots', 'filter_wpseo_robots', 10, 1 );

function redirect_old_sport_types(){
    $post_id = url_to_postid( $_SERVER['REQUEST_URI'] , '_wpg_def_keyword', true );
    $cats = wp_get_post_categories( $post_id );

    if( in_array('49', $cats) && strtotime(get_the_date('Y-m-d', $post_id)) < strtotime('31-12-2019')){
        header("HTTP/1.1 301 Moved Permanently");
        header("Location: ". get_the_permalink(7395) ."");
        exit();
    }
}

function search_for_faq_block(){
    $context_menu_list = array();
    global $post;
    if ( has_blocks( $post->post_content ) ) {
        $blocks = parse_blocks( $post->post_content );
        foreach ($blocks as $single){
            if ($single['blockName'] == 'yoast/faq-block') {
                $i = 0;
                $len = count($single['attrs']['questions']);
                ?>
                <script type="application/ld+json">
                {
                "@context": "https://schema.org",
                "@type": "FAQPage",
                "mainEntity": [
                    <?php  foreach($single['attrs']['questions'] as $question): ?>
                    {
                    "@type": "Question",
                    "name": <?php echo json_encode($question['jsonQuestion']); ?>,
                    "acceptedAnswer": {
                    "@type": "Answer",
                    "text": <?php echo json_encode($question['jsonAnswer']); ?>
                    }}<?php echo ($i == $len - 1) ? '' : ','; ?>
                    
                    <?php $i++; endforeach; ?>
                
                ]}
                </script>
                <?php
            }

        }

    }

    return $context_menu_list;
}
function list_context_headers_for_menu($id){
    $post = get_post($id);
    $context_menu_list = array();

    if ( has_blocks( $post->post_content ) ) {
        $blocks = parse_blocks( $post->post_content );
        foreach ($blocks as $single){
            if ( $single['blockName'] === 'block-lab/header-context-menu-bar' ) {
                // var_dump($single['attrs']);exit();
                $context_menu_list[] = array(
                    'id' => sanitize_title($single['attrs']['skrcony-header']),
                    'name' => $single['attrs']['skrcony-header']
                );
            }
        }

    }

    return $context_menu_list;
}
function auto_id_headings( $content ) {
	$content = preg_replace_callback( '/(\<h[1-6](.*?))\>(.*)(<\/h[1-2]>)/i', function( $matches ) {
		if ( ! stripos( $matches[0], 'id=' ) ) :
			$matches[0] = $matches[1] . $matches[2] . ' class="scroll-to-target" id="' . sanitize_title( $matches[3] ) . '">' . $matches[3] . $matches[4];
		endif;
		return $matches[0];
	}, $content );
    return $content;
}

function get_heading_list($id){
    $content = get_post_field('post_content', $id);
     $list = array();
     global $list;
    $content = preg_replace_callback( '/(\<h[1-6](.*?))\>(.*)(<\/h[1-2]>)/i', function( $matches ) {
		if ( ! stripos( $matches[0], 'id=' ) ) :
            $matches[0] = $matches[1] . $matches[2] . ' id="' . sanitize_title( $matches[3] ) . '">' . $matches[3] . $matches[4];
            global $list;
            $list[]  = array(
                'id' => sanitize_title( $matches[3] ),
                'title' => $matches[3]
            );
        endif;
		return $matches[0];
	}, $content );

    return $list;
}




function na_remove_slug( $post_link, $post, $leavename ) {

    if ( 'bookmaker' != $post->post_type || 'publish' != $post->post_status ) {
        return $post_link;
    }

    $post_link = str_replace( '/' . $post->post_type . '/', '/', $post_link );

    return $post_link;
}

function na_parse_request( $query ) {

    if ( ! $query->is_main_query() || 2 != count( $query->query ) || ! isset( $query->query['page'] ) ) {
        return;
    }

    if ( ! empty( $query->query['name'] ) ) {
        $query->set( 'post_type', array( 'post', 'bookmaker', 'page' ) );
    }
}


function show_pending_number($menu) {
    $types = array("review");
    $status = "pending";
    foreach($types as $type) {
        $num_posts = wp_count_posts($type, 'readable');
        $pending_count = 0;
        if (!empty($num_posts->$status)) $pending_count = $num_posts->$status;

        if ($type == 'post') {
            $menu_str = 'edit.php';
        } else {
            $menu_str = 'edit.php?post_type=' . $type;
        }

        foreach( $menu as $menu_key => $menu_data ) {
            if( $menu_str != $menu_data[2] )
                continue;
            $menu[$menu_key][0] .= " <span class='update-plugins count-$pending_count'><span class='plugin-count'>"
                . number_format_i18n($pending_count)
                . '</span></span>';
        }
    }
    return $menu;
}
add_filter('add_menu_classes', 'show_pending_number');

function noindex_search() {
	echo '<meta name="robots" content="noindex,follow"/>';
}
if ( isset( $_GET['s'] ) ) {
	add_action('wp_head', 'noindex_search');
}

function google_site_ver() {
	echo '<meta name="google-site-verification" content="7LoxTjYgzasZ91q7rvZzS5AknzLEa6vTTt5BUuKzsNs" />';
}
add_action('wp_head', 'google_site_ver');

function ajax_save_csg_review()
{

    $params = array();
    parse_str($_POST['data'], $params);

    if (!$params['name']) {
        echo json_encode(array('type' => 'error', 'message' => __('Fill all required fields', 'gcompare')));
        exit();
    }

    $my_cptpost_args = array(

        'post_title' => 'opinia ' . date('m/d/Y h:i:s a', time()),
        'post_content' => $params['message-positive'].'</br>'.$params['message-negative'],
        'post_status' => 'pending',
				'post_type' => 'review',
				'post_author' =>'8',
        'meta_input' => array(
            'name' => $params['name'],
            '_name' => 'field_5e31de372949c',
            'pozytywna_opinia' => $params['message-positive'],
            '_pozytywna_opinia' => 'field_5e31ddd20280c',
            'negatywna_opinia' => $params['message-negative'],
            '_negatywna_opinia' => 'field_5e31dde10280d',
            'ilosc_gwiazdek' => (round($params['rate'] * 2) / 2),
            '_ilosc_gwiazdek' => 'field_5e31ddc80280b',
            'related_post_id' => $params['post_id'],
            '_related_post_id' => 'field_5e31ddfe0280e',
        )

    );

    $cpt_id = wp_insert_post($my_cptpost_args);
    if ($cpt_id) {
        echo json_encode(array('type' => 'success', 'message' => __('Twoja opinia za chwilę pojawi się na stronie.', 'gcompare'), 'post_id' => $params['post_id']));
        exit();
    } else {
        echo json_encode(array('type' => 'error', 'message' => __('Przepraszamy, Twoja opinia nie została zapisana. Spróbuj ponownie.', 'gcompare')));
        exit();
    }
}


function promocodes_post_type()
{
    {

// Set UI labels for Custom Post Type
        $labels = array(
            'name' => _x('Kody promocyjne', 'Post Type General Name'),
            'singular_name' => _x('kod promocyjny', 'Post Type Singular Name'),
            'menu_name' => __('kody promocyjne'),
            'parent_item_colon' => __('Nadrzędny'),
            'all_items' => __('Wszystkie'),
            'view_item' => __('Wyświetl'),
            'add_new_item' => __('Dodaj nowego'),
            'add_new' => __('Dodaj nowego'),
            'edit_item' => __('Edytuj'),
            'update_item' => __('Zaktualizuj'),
            'search_items' => __('Szukaj'),
            'not_found' => __('Nie znaleziono'),
            'not_found_in_trash' => __('Nie znaleziono w koszu'),
        );

// Set other options for Custom Post Type

        $args = array(
            'label' => __('promocode'),
            'description' => __('kody promocyjne'),
            'labels' => $labels,
            // Features this CPT supports in Post Editor
            'supports' => array('title', 'editor'),
            // You can associate this CPT with a taxonomy or custom taxonomy.
            /* A hierarchical CPT is like Pages and can have
            * Parent and child items. A non-hierarchical CPT
            * is like Posts.
            */
            'hierarchical' => false,
            'public' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'show_in_nav_menus' => true,
            'show_in_admin_bar' => true,
            'menu_position' => 5,
            'can_export' => true,
            'has_archive' => false,
            'show_in_rest' => true,
            'exclude_from_search' => false,
            'publicly_queryable' => true,
            'capability_type' => 'post',
            'menu_icon' => 'dashicons-editor-ul'
        );

        // Registering your Custom Post Type
        register_post_type('promocode', $args);
    }
}
function bookmakers_post_type()
{
    {

// Set UI labels for Custom Post Type
        $labels = array(
            'name' => _x('Bukmacherzy', 'Post Type General Name'),
            'singular_name' => _x('Bukmacher', 'Post Type Singular Name'),
            'menu_name' => __('Bukmacherzy'),
            'parent_item_colon' => __('Nadrzędny Bukmacher'),
            'all_items' => __('Wszyscy'),
            'view_item' => __('Wyświetl Bukmachera'),
            'add_new_item' => __('Dodaj nowego'),
            'add_new' => __('Dodaj nowego'),
            'edit_item' => __('Edytuj bukmachera'),
            'update_item' => __('Zaktualizuj bukmachera'),
            'search_items' => __('Szukaj bukmachera'),
            'not_found' => __('Nie znaleziono'),
            'not_found_in_trash' => __('Nie znaleziono w koszu'),
        );

// Set other options for Custom Post Type

        $args = array(
            'label' => __('bookmaker'),
            'description' => __('bukmacherzy'),
            'labels' => $labels,
            // Features this CPT supports in Post Editor
            'supports' => array('title', 'editor', 'thumbnail'),
            // You can associate this CPT with a taxonomy or custom taxonomy.
            /* A hierarchical CPT is like Pages and can have
            * Parent and child items. A non-hierarchical CPT
            * is like Posts.
            */
            'hierarchical' => false,
            'public' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'show_in_nav_menus' => true,
            'show_in_admin_bar' => true,
            'menu_position' => 5,
            'can_export' => true,
            'has_archive' => false,
            'show_in_rest' => true,
            'exclude_from_search' => false,
            'publicly_queryable' => true,
            'capability_type' => 'post',
            'menu_icon' => 'dashicons-admin-site'
        );

        // Registering your Custom Post Type
        register_post_type('bookmaker', $args);
    }
}

function reviews_post_type()
{
    {

// Set UI labels for Custom Post Type
        $labels = array(
            'name' => _x('Opinie', 'Post Type General Name'),
            'singular_name' => _x('Opinia', 'Post Type Singular Name'),
            'menu_name' => __('Opinie'),
            'parent_item_colon' => __('Nadrzędna opinia'),
            'all_items' => __('Wszystkie opinie'),
            'view_item' => __('Wyświetl opinię'),
            'add_new_item' => __('Dodaj nową opinię'),
            'add_new' => __('Dodaj nową'),
            'edit_item' => __('Edytuj opinię'),
            'update_item' => __('Zaktualizuj opinię'),
            'search_items' => __('Szukaj opinii'),
            'not_found' => __('Nie znaleziono'),
            'not_found_in_trash' => __('Nie znaleziono w koszu'),
        );

// Set other options for Custom Post Type

        $args = array(
            'label' => __('review'),
            'description' => __('Produkty finansowe'),
            'labels' => $labels,
            // Features this CPT supports in Post Editor
            'supports' => array('title', 'editor'),
            // You can associate this CPT with a taxonomy or custom taxonomy.
            /* A hierarchical CPT is like Pages and can have
            * Parent and child items. A non-hierarchical CPT
            * is like Posts.
            */
            'hierarchical' => false,
            'public' => false,
            'show_ui' => true,
            'show_in_menu' => true,
            'show_in_nav_menus' => true,
            'show_in_admin_bar' => true,
            'menu_position' => 5,
            'can_export' => true,
            'has_archive' => false,
            'exclude_from_search' => false,
            'publicly_queryable' => true,
            'capability_type' => 'post',
            'menu_icon' => 'dashicons-admin-comments'
        );

        // Registering your Custom Post Type
        register_post_type('review', $args);
    }
}

function legal_redirect_attachment_page() {
	if ( is_attachment() ) {
		global $post;
		if ( $post && $post->post_parent ) {
			wp_redirect( esc_url( get_permalink( $post->post_parent ) ), 301 );
			exit;
		} else {
			wp_redirect( esc_url( home_url( '/' ) ), 301 );
			exit;
		}
	}
}
add_action( 'template_redirect', 'legal_redirect_attachment_page' );

// CSG FUNCTION END
add_action( 'wp_footer', function () { ?>
	<script>
  if ( 'querySelectorAll' in document) {

	(function() {
	const parents  = document.querySelectorAll('.wp-block-yoast-faq-block');
	Array.prototype.forEach.call(parents, parent => {
		parent.classList.add('yoast-faq-accordion');
	});
	// Get all the questions
	const headings = document.querySelectorAll('.yoast-faq-accordion .schema-faq-question')

	Array.prototype.forEach.call(headings, heading => {
	  // Give each question a toggle button child
	  // with the SVG plus/minus icon
	  heading.innerHTML = `
		<button aria-expanded="false">
		  ${heading.textContent}
		  <svg aria-hidden="true" focusable="false" viewBox="0 0 10 10">
			<rect class="vert" height="8" width="2" y="1" x="4"/>
			<rect height="2" width="8" y="4" x="1"/>
		  </svg>
		</button>
	  `;

	  // Function to create a node list
	  // of the content between this question and the next
	  const getContent = (elem) => {
		let elems = [];
		while (elem.nextElementSibling && elem.nextElementSibling.className === 'schema-faq-answer') {
		  elems.push(elem.nextElementSibling);
		  elem = elem.nextElementSibling;
		}

		// Delete the old versions of the content nodes
		elems.forEach((node) => {
		  node.parentNode.removeChild(node);
		})

		return elems;
	  }

	  // Assign the contents to be expanded/collapsed (array)
	  let contents = getContent(heading);

	  // Create a wrapper element for `contents` and hide it
	  let wrapper = document.createElement('div');
	  wrapper.hidden = true;

	  // Add each element of `contents` to `wrapper`
	  contents.forEach(node => {
		wrapper.appendChild(node);
	  })

	  // Add the wrapped content back into the DOM
	  // after the question
	  heading.parentNode.insertBefore(wrapper, heading.nextElementSibling);

	  // Assign the button
	  let btn = heading.querySelector('button');

	  btn.onclick = () => {
		// Cast the state as a boolean
		let expanded = btn.getAttribute('aria-expanded') === 'true' || false;

		// Switch the state
		btn.setAttribute('aria-expanded', !expanded);
		// Switch the content's visibility
		wrapper.hidden = expanded;
	  }
	})
  })()

  }
	</script>
<?php } );
