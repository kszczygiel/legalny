<?php

$slug_url = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

// Blog prefix start

$slug_url_exploded = explode('/',$_SERVER['REQUEST_URI']);
end($slug_url_exploded);
$slug_parameter = prev($slug_url_exploded);

function get_id_by_slug($page_slug) {
	$page = get_page_by_path($page_slug);
	if ($page) {
		return $page->ID;
	} else {
		return null;
	}
}

$post_by_slug = get_id_by_slug($slug_parameter);

if ($post_by_slug !== NULL) {
	$correct_post_permalink = get_permalink($post_by_slug);

	$get_parameter = $_GET;
	$get_string = '';

	if(!empty($get_parameter)) {

		$i = 0;

		foreach ($get_parameter as $key=>$single_parameter) {
			if ($i !== 0) {
				$get_string .= '&' . $key . '=' . $single_parameter;
			} else {
				$get_string .= '?' . $key . '=' . $single_parameter;
			}

			$i++;
		}
	}


//	wp_redirect($correct_post_permalink . $get_string);
}

$context = Timber::get_context();
Timber::render('views/templates/404.twig', $context);
