<?php
/*
Template Name: Poradnik single
Template Post Type: post, page
*/

$context = Timber::get_context();
$context['post'] = Timber::get_post();

$context['headings_list'] = get_heading_list($context['post']->ID);

$args = [
	'numberposts'	=> 1,
	'post_type'		=> 'post',
	'meta_key'		=> 'wyroznione',
  'meta_value'	=> '1',
  'category' => array('112')
];

$context['poradnik_main'] = Timber::get_posts($args);

$not_in = array();

$not_in[] = $context['post']->ID;

if(!$context['poradnik_main']){
  $args = [
    'numberposts'	=> 1,
    'post_type'		=> 'post',
    'meta_key'		=> 'wyroznione',
    'meta_value'	=> '0',
    'category' => array('112'),
    'post__not_in' => $not_in,

  ];

  $context['poradnik_main'] = Timber::get_posts($args);

}


$not_in[] = $context['poradnik_main'][0]->ID;

$args = [
  'numberposts' => 2,
  'category' => array('112'),
  'meta_key'		=> 'wyroznione',
  'meta_value'	=> '0',
  'post__not_in' => $not_in,
];

$context['poradniki'] = Timber::get_posts($args);


$context['poradniki_link'] = get_category_link('112');



Timber::render('views/templates/poradnik-single.twig', $context);
