<?php
/*
Template Name: Poradnik lista
Template Post Type: post, page

*/

$slug_url = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

if (strpos($slug_url, '/page/')) {
	$slug_url_exploded = explode('/',$_SERVER['REQUEST_URI']);
	$slug_url_exploded_removed = array_splice($slug_url_exploded, 0,-3);
	$slug_url_imploded =  implode('/',$slug_url_exploded_removed);
	$redirect_link = "https://$_SERVER[HTTP_HOST]".$slug_url_imploded . '/';
	wp_redirect($redirect_link);
}

$context = Timber::get_context();
$context['post'] = Timber::get_post();

foreach (get_field('powiazane_tagi') as $tag){
  $tag['tag'] = get_tag($tag['tag'][0]);

  $args = [
	'post_type' => 'post',
	'nopaging' => true,
	'tag' => $tag['tag']->slug,
	'category' => array('112')
  ];

  $tag['posts'] = Timber::get_posts($args);
  $context['powiazane_tagi'][] = $tag;
}

Timber::render('views/templates/poradnik-lista.twig', $context);
