<?php
/*
Template Name: Porównanie
Template Post Type: post, page
*/

$context = Timber::get_context();
$context['post'] = Timber::get_post();
$args = [
  'numberposts' => 2,
  'category' => array('283')
];

$context['headings_list'] = get_heading_list($context['post']->ID);
$context['buk_lewy'] = Timber::get_post(get_field('bukmacher_lewo'));
$context['buk_prawy'] = Timber::get_post(get_field('bukmacher_prawo'));

Timber::render('views/templates/porownanie.twig', $context);
