  <?php

$context = Timber::get_context();
$context['post'] = Timber::get_post();
$args = [
  'numberposts' => 2,
  'category' => array('283')
];

$context['recent'] = Timber::get_posts($args);
$context['recent_link'] = get_category_link('283');
$context['context_list'] = list_context_headers_for_menu($context['post']->ID);

$context['kod_bonusowy'] = Timber::get_posts(array(
  'numberposts'	=> -1,
  'post_type'		=> 'post',
  'meta_key'		=> 'powiazany_bukmacher',
  'meta_value'	=>  $context['post']->ID
))[0];

$args = [
  'posts_per_page' => -1,
  'post_type' => 'review',
  'post_status' => 'publish',
  'numberposts' => -1,
  'meta_key' => 'related_post_id',
  'meta_value' => $context['post']->ID
];

$context['hide_review_form'] = !in_array($context['post']->ID,explode(',',$_COOKIE['BlockReview']));


$context['reviews'] = Timber::get_posts($args);

Timber::render('views/templates/recenzja.twig', $context);
