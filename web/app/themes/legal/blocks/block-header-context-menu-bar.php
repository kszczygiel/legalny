<?php
$header = block_field( 'header', false );
$short_header= block_field( 'skrcony-header', false );
$heading= block_field( 'naglowek', false );

$html = '<div id="'.sanitize_title($short_header).'" class="context-header"><'.$heading.'>'. $header .'</'.$heading.'></div>';

echo $html;