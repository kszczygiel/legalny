<?php
$context = Timber::get_context();
$context['link']  = block_field( 'link-buttona', false );
$context['text'] = block_field( 'tekst-buttona', false );

Timber::render('views/blocks/buks-button-center.twig', $context);
