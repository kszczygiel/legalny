<?php

$context = Timber::get_context();
$context['lewo_gwiazdki'] = block_field( 'lewo-ilo-gwiazdek', false );
$context['prawo_gwiazdki'] = block_field( 'prawo-ilo-gwiazdek', false );
$curr_post = get_post();

$context['buk_lewo'] = Timber::get_post(get_field('bukmacher_lewo',$curr_post->ID)) ;
$context['buk_prawo'] = Timber::get_post(get_field('bukmacher_prawo',$curr_post->ID)) ;

Timber::render('views/blocks/buks-versus-verdict.twig', $context);
