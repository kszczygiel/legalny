<?php

$context = Timber::get_context();
$context['block_num'] = block_field( 'ilo-kafelkow', false );

foreach($context['options']['lista_bukmacherow'] as $single){
  $context['lista_bukmacherow'][] = Timber::get_post($single['bukmacher']->ID);
}

Timber::render('views/blocks/buks-table-full.twig', $context);
