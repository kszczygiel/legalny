<?php

$context = Timber::get_context();
$all_buks = block_field( 'bukmacherzy', false )['rows'];

$context['all_buks'] = array();



foreach ($all_buks as $buk){
  $context['all_buks'][$buk['bukmacher']['id']] = Timber::get_post($buk['bukmacher']['id']);
  $context['all_buks'][$buk['bukmacher']['id']]->kwota_1 = $buk['kwota-pierwsza'];
  $context['all_buks'][$buk['bukmacher']['id']]->kwota_1_tekst = $buk['kwota-pierwsza-tekst'];
  $context['all_buks'][$buk['bukmacher']['id']]->kwota_2 = $buk['kwota-druga'];
  $context['all_buks'][$buk['bukmacher']['id']]->kwota_2_tekst = $buk['kwota-druga-tekst'];
}

Timber::render('views/blocks/buks-bars-smaller-cash.twig', $context);
