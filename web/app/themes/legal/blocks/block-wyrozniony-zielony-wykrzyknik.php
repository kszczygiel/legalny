<?php

$context = Timber::get_context();
$context['content'] = block_field( 'tresc', false );
$context['title'] = block_field( 'tytul', false );


Timber::render('views/blocks/buks-green-exclamation.twig', $context);
