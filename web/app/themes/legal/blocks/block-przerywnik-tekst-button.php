<?php

$context = Timber::get_context();
$context['title'] = block_field( 'tytul', false );
$context['button_text'] = block_field( 'tekst-buttona', false );
$context['button_url'] = block_field( 'link-buttona', false );
$context['bg'] = block_field( 'tlo', false );
$context['bg_color'] = block_field( 'kolor-tla', false );
$context['dark_button'] = block_field( 'ciemny_przycisk', false );

Timber::render('views/blocks/buks-intersection.twig', $context);
