<?php

$context = Timber::get_context();
$all_buks = block_field( 'bukmacherzy', false )['rows'];

$context['all_buks'] = array();

if($post->ID == 4582){
  $context['close_div'] = 1;
}

foreach ($all_buks as $buk){
  $context['all_buks'][] = Timber::get_post($buk['bukmacher']['id']);
}

Timber::render('views/blocks/buks-bars-standard.twig', $context);
