<?php

$context = Timber::get_context();
$context['title'] = block_field( 'tytul', false );
$context['step_1_text'] = block_field( 'krok-1-tekst', false );
$context['code'] = block_field( 'kod-prezentu', false );
$context['step_2_text'] = block_field( 'krok-2-tekst', false );
$context['step_2_btn'] = block_field( 'krok-2-tekst-buttona', false );
$context['step_2_url'] = block_field( 'krok-2-link-buttona', false );
$context['step_3_text'] = block_field( 'krok-3-tekst', false );
$context['step_3_btn'] = block_field( 'krok-3-tekst-buttona', false );
$context['step_3_url'] = block_field( 'krok-3-link-buttona', false );
$context['bg'] = block_field( 'tlo', false );
$context['color'] = block_field( 'kolor', false );

Timber::render('views/blocks/buks-article-steps.twig', $context);
