<?php

$context = Timber::get_context();
$context['naglowki'] = get_field('naglowki');
$context['belki'] = get_field('belki');

foreach($context['belki'] as $single){
  $single['bukmacher'] = Timber::get_post($single['bukmacher']->ID);
}

Timber::render('views/blocks/bar-rates.twig', $context);
