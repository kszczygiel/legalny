<?php

$context = Timber::get_context();
$context['url'] = block_field( 'link', false );
$context['title'] = block_field( 'tytul', false );
$context['short_desc'] = block_field( 'krotki-opis', false );
$context['with_image'] = block_field( 'box-po-lewej', false );


Timber::render('views/blocks/buks-article-sneakpeak.twig', $context);
