<?php

$context = Timber::get_context();
$context['image'] = block_field( 'zdjecie', false );
$context['button_text'] = block_field( 'button-tekst', false );
$context['button_url'] = block_field( 'button-url', false );
$curr_post = get_post();
if(get_post_type($curr_post->ID) != 'bookmaker'){
  $context['color'] = get_field('powiazany_bukmacher',$curr_post->ID) ;

  if(!$context['color']){
    $context['color'] =  get_field('bukmacher_sidebar', $curr_post->ID) ;
  }
}
else{
  $context['color'] = $curr_post;
}

Timber::render('views/blocks/buks-image-colorbg.twig', $context);
