<?php

$context = Timber::get_context();
$buk = block_field( 'bukmacher', false );
$context['text'] = block_field( 'dodatkowy-tekst-po-prawej', false );
$context['kwoty'] = block_field( 'kwoty', false )['rows'];

foreach($context['kwoty'] as &$kwota){
  $kwota['kwota_tekst'] = $kwota['kwota-tekst'];
}

$context['all_buks'] = array();
$context['item'] = Timber::get_post($buk->ID);


Timber::render('views/blocks/buks-bars-small-single.twig', $context);
