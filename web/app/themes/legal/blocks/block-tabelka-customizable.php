<?php

$context = Timber::get_context();
$context['bukmacherzy'] = block_field( 'bukmacherzy', false );
$context['go_btn_text'] = block_field( 'tekst-buttona-go', false );


foreach($context['bukmacherzy']['rows'] as $single){
  // var_dump($single);exit();
  $context['lista_bukmacherow'][$single['bukmacher']['id']] = Timber::get_post($single['bukmacher']['id']);
  $context['lista_bukmacherow'][$single['bukmacher']['id']]->custom_text_table = $single['tekst'];
  $context['lista_bukmacherow'][$single['bukmacher']['id']]->custom_text_table_second = $single['tekst-druga-kolumna'];
  $context['lista_bukmacherow'][$single['bukmacher']['id']]->custom_stara_kwota = $single['stara-kwota'];
  $context['lista_bukmacherow'][$single['bukmacher']['id']]->custom_nowa_kwota = $single['nowa-kwota'];
}

Timber::render('views/blocks/buks-table-custom.twig', $context);
