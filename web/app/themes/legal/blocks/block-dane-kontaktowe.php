<?php

$context = Timber::get_context();
$context['phone'] = block_field( 'telefon', false );
$context['mail'] = block_field( 'mail', false );
$context['language'] = block_field( 'jezyki', false );
$context['availability'] = block_field( 'dostepnosc', false );

Timber::render('views/blocks/buks-contact-data.twig', $context);
