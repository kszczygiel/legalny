<?php

$context = Timber::get_context();
$context['zdjcie_prawe'] = block_field( 'zdjcie-prawe', false );
$context['zdjcie_lewe'] = block_field( 'zdjcie-lewe', false );
$curr_post = get_post();

$context['buk_lewo'] = Timber::get_post(get_field('bukmacher_lewo',$curr_post->ID)) ;
$context['buk_prawo'] = Timber::get_post(get_field('bukmacher_prawo',$curr_post->ID)) ;

Timber::render('views/blocks/buks-image-colorbg-versus.twig', $context);
