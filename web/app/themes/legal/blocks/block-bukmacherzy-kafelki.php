<?php

$context = Timber::get_context();
$context['block_num'] = block_field( 'ilo-kafelkow', false );
$context['link_type'] = block_field( 'linkowanie', false );

foreach($context['options']['kafelki_bukmacherzy'] as $single){
  $context['kafelki_bukmacherzy'][] = Timber::get_post($single['bukmacher']->ID);
}

Timber::render('views/blocks/buks-tiles.twig', $context);
