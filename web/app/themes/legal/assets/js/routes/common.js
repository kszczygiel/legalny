export default {
  init() {
    // JavaScript to be fired on all pages
    $(document).foundation();

    function _classCallCheck(t, i) { if (!(t instanceof i)) throw new TypeError("Cannot call a class as a function") } var Sticky = function () { function t() { var i = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "", e = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {}; _classCallCheck(this, t), this.selector = i, this.elements = [], this.version = "1.2.0", this.vp = this.getViewportSize(), this.body = document.querySelector("body"), this.options = { wrap: e.wrap || !1, marginTop: e.marginTop || 0, stickyFor: e.stickyFor || 0, stickyClass: e.stickyClass || null, stickyContainer: e.stickyContainer || "body" }, this.updateScrollTopPosition = this.updateScrollTopPosition.bind(this), this.updateScrollTopPosition(), window.addEventListener("load", this.updateScrollTopPosition), window.addEventListener("scroll", this.updateScrollTopPosition), this.run() } return t.prototype.run = function () { var t = this, i = setInterval(function () { if ("complete" === document.readyState) { clearInterval(i); var e = document.querySelectorAll(t.selector); t.forEach(e, function (i) { return t.renderElement(i) }) } }, 10) }, t.prototype.renderElement = function (t) { var i = this; t.sticky = {}, t.sticky.active = !1, t.sticky.marginTop = parseInt(t.getAttribute("data-margin-top")) || this.options.marginTop, t.sticky.stickyFor = parseInt(t.getAttribute("data-sticky-for")) || this.options.stickyFor, t.sticky.stickyClass = t.getAttribute("data-sticky-class") || this.options.stickyClass, t.sticky.wrap = !!t.hasAttribute("data-sticky-wrap") || this.options.wrap, t.sticky.stickyContainer = this.options.stickyContainer, t.sticky.container = this.getStickyContainer(t), t.sticky.container.rect = this.getRectangle(t.sticky.container), t.sticky.rect = this.getRectangle(t), "img" === t.tagName.toLowerCase() && (t.onload = function () { return t.sticky.rect = i.getRectangle(t) }), t.sticky.wrap && this.wrapElement(t), this.activate(t) }, t.prototype.wrapElement = function (t) { t.insertAdjacentHTML("beforebegin", "<span></span>"), t.previousSibling.appendChild(t) }, t.prototype.activate = function (t) { t.sticky.rect.top + t.sticky.rect.height < t.sticky.container.rect.top + t.sticky.container.rect.height && t.sticky.stickyFor < this.vp.width && !t.sticky.active && (t.sticky.active = !0), this.elements.indexOf(t) < 0 && this.elements.push(t), t.sticky.resizeEvent || (this.initResizeEvents(t), t.sticky.resizeEvent = !0), t.sticky.scrollEvent || (this.initScrollEvents(t), t.sticky.scrollEvent = !0), this.setPosition(t) }, t.prototype.initResizeEvents = function (t) { var i = this; t.sticky.resizeListener = function () { return i.onResizeEvents(t) }, window.addEventListener("resize", t.sticky.resizeListener) }, t.prototype.destroyResizeEvents = function (t) { window.removeEventListener("resize", t.sticky.resizeListener) }, t.prototype.onResizeEvents = function (t) { this.vp = this.getViewportSize(), t.sticky.rect = this.getRectangle(t), t.sticky.container.rect = this.getRectangle(t.sticky.container), t.sticky.rect.top + t.sticky.rect.height < t.sticky.container.rect.top + t.sticky.container.rect.height && t.sticky.stickyFor < this.vp.width && !t.sticky.active ? t.sticky.active = !0 : (t.sticky.rect.top + t.sticky.rect.height >= t.sticky.container.rect.top + t.sticky.container.rect.height || t.sticky.stickyFor >= this.vp.width && t.sticky.active) && (t.sticky.active = !1), this.setPosition(t) }, t.prototype.initScrollEvents = function (t) { var i = this; t.sticky.scrollListener = function () { return i.onScrollEvents(t) }, window.addEventListener("scroll", t.sticky.scrollListener) }, t.prototype.destroyScrollEvents = function (t) { window.removeEventListener("scroll", t.sticky.scrollListener) }, t.prototype.onScrollEvents = function (t) { t.sticky.active && this.setPosition(t) }, t.prototype.setPosition = function (t) { this.css(t, { position: "", width: "", top: "", left: "" }), this.vp.height < t.sticky.rect.height || !t.sticky.active || (t.sticky.rect.width || (t.sticky.rect = this.getRectangle(t)), t.sticky.wrap && this.css(t.parentNode, { display: "block", width: t.sticky.rect.width + "px", height: t.sticky.rect.height + "px" }), 0 === t.sticky.rect.top && t.sticky.container === this.body ? this.css(t, { position: "fixed", top: t.sticky.rect.top + "px", left: t.sticky.rect.left + "px", width: t.sticky.rect.width + "px" }) : this.scrollTop > t.sticky.rect.top - t.sticky.marginTop ? (this.css(t, { position: "fixed", width: t.sticky.rect.width + "px", left: t.sticky.rect.left + "px" }), this.scrollTop + t.sticky.rect.height + t.sticky.marginTop > t.sticky.container.rect.top + t.sticky.container.offsetHeight ? (t.sticky.stickyClass && t.classList.remove(t.sticky.stickyClass), this.css(t, { top: t.sticky.container.rect.top + t.sticky.container.offsetHeight - (this.scrollTop + t.sticky.rect.height) + "px" })) : (t.sticky.stickyClass && t.classList.add(t.sticky.stickyClass), this.css(t, { top: t.sticky.marginTop + "px" }))) : (t.sticky.stickyClass && t.classList.remove(t.sticky.stickyClass), this.css(t, { position: "", width: "", top: "", left: "" }), t.sticky.wrap && this.css(t.parentNode, { display: "", width: "", height: "" }))) }, t.prototype.update = function () { var t = this; this.forEach(this.elements, function (i) { i.sticky.rect = t.getRectangle(i), i.sticky.container.rect = t.getRectangle(i.sticky.container), t.activate(i), t.setPosition(i) }) }, t.prototype.destroy = function () { var t = this; this.forEach(this.elements, function (i) { t.destroyResizeEvents(i), t.destroyScrollEvents(i), delete i.sticky }) }, t.prototype.getStickyContainer = function (t) { for (var i = t.parentNode; !i.hasAttribute("data-sticky-container") && !i.parentNode.querySelector(t.sticky.stickyContainer) && i !== this.body;)i = i.parentNode; return i }, t.prototype.getRectangle = function (t) { this.css(t, { position: "", width: "", top: "", left: "" }); var i = Math.max(t.offsetWidth, t.clientWidth, t.scrollWidth), e = Math.max(t.offsetHeight, t.clientHeight, t.scrollHeight), s = 0, o = 0; do s += t.offsetTop || 0, o += t.offsetLeft || 0, t = t.offsetParent; while (t); return { top: s, left: o, width: i, height: e } }, t.prototype.getViewportSize = function () { return { width: Math.max(document.documentElement.clientWidth, window.innerWidth || 0), height: Math.max(document.documentElement.clientHeight, window.innerHeight || 0) } }, t.prototype.updateScrollTopPosition = function () { this.scrollTop = (window.pageYOffset || document.scrollTop) - (document.clientTop || 0) || 0 }, t.prototype.forEach = function (t, i) { for (var e = 0, s = t.length; e < s; e++)i(t[e]) }, t.prototype.css = function (t, i) { for (var e in i) i.hasOwnProperty(e) && (t.style[e] = i[e]) }, t }(); !function (t, i) { "undefined" != typeof exports ? module.exports = i : "function" == typeof define && define.amd ? define([], i) : t.Sticky = i }(this, Sticky);

    //init
    var sticky = new Sticky('.sticky');

    if ($('.title-menu').length) {
      $('.title-menu').attr('data-offset-top', $('.title-menu').offset().top);
    }

    initSlickOnTypes();

    var headingsHeight = [];
    setHeadingsHeightArray();
    function setHeadingsHeightArray() {
      $('.context-list li').each(function (e) {
        headingsHeight.push(
          {
            'class': $(this).find('a').attr('class'),
            'offtop': $('#' + $(this).find('a').attr('class')).offset().top
          }
        )
      });
    }

    var isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);

    if(isSafari){
      $('.btn.safari-width').each(function(){
        var width =  ($(this).find('>span').width() + parseInt($(this).css('padding-left')) + parseInt($(this).css('padding-right')) );
       $(this).css('min-width', width +'px')
     });
    }


    //click

    $('.buks-table .row .vote-btn').click(function(){
      if(!$(this).hasClass('disabled')){
          var elem  = $(this).parents('.row').find('.score-vote .score .count');    
          var value = parseInt(elem.html()) + 1 + '&nbsp;';
          $('.buks-table .row .vote-btn').addClass('disabled');
          elem.html(value);
      }
      })

    $('.legal-bookmaker .expand-btn').click(function () {
      if ($(this).siblings('.hidden-content').hasClass('expanded')) {
        $(this).removeClass('expanded').siblings('.hidden-content').removeClass('expanded').slideUp(500)
      }
      else {
        $(this).addClass('expanded').siblings('.hidden-content').addClass('expanded').slideDown(500)
      }
    });


    $('.btn-info').click(function () {
      $('.btn-info').not(this).removeClass('expanded').parents('.bulletpoints').find('>.bulletpoints-content').slideUp(700);
      if ($(this).hasClass('expanded')) {
        var classAction = 'removeClass';
        var toggleAction = 'slideUp';
      }
      else {
        var classAction = 'addClass';
        var toggleAction = 'slideDown';
      }
      var parent = $(this).parents('.bulletpoints');
      $(this)[classAction]('expanded');
      parent.find('>.bulletpoints-content')[toggleAction](700);
    });

    $('.menu-mobile-btn').click(function () {
      var menu = $('.main-header .menu .header-menu');

      if (menu.hasClass('expanded')) {
        menu.removeClass('expanded');
        menu.find('.nav-drop').removeClass('expanded');
      }
      else {
        menu.addClass('expanded');
      }
    });

    $('.menu-item-has-children').click(function () {
      if ($(window).width() < 1255) {
        $(this).find('.nav-drop').toggleClass('expanded');
      }
    });


    $('.menu-item-has-children .back').click(function (e) {
      e.stopImmediatePropagation();
      if ($(window).width() < 1255) {
        $(this).parents('.nav-drop').removeClass('expanded');
      }
    });
    $('.menu-item-has-children a').click(function (e) {
      e.stopImmediatePropagation();
    });

    $('.copy-code').click(function (e) {
      e.preventDefault();
      e.stopImmediatePropagation();
      var dummyContent = $(this).attr('data-code');
      var dummy = $('<input>').val(dummyContent).appendTo('body').select()
      document.execCommand('copy');
      new Noty({
        type: 'success',
        text: 'Skopiowano do showka',
        progressBar: true,
        theme: 'mint',
        timeout: 3000
      }).show();
      dummy.remove();
    });
    $('#dodajOpinie').submit(function (e) {
      e.preventDefault();
    })

    $('#dodajOpinie button').click(function () {
      $('#dodajOpinie button').addClass('loading').html('<i class="fa fa-spinner fa-spin" style="color:white;"></i>');
      jQuery.ajax({
        url: ajaxurl,
        data: {
          action: 'ajax_save_review',
          data: $('#dodajOpinie').serialize()
        },
        type: 'POST',
        success: function (data) {
          var info = JSON.parse(data);
          $('#dodajOpinie button').removeClass('loading').html('DODAJ OPINIĘ');
          new Noty({
            type: info.type,
            text: info.message,
            progressBar: true,
            theme: 'mint',
            timeout: 3000
          }).show();
          if (info.type == "success") {
            $('#dodajOpinie').remove();
            setCookie('BlockReview', getCookie('BlockReview') + ',' + info.post_id, 365);
          }
        }
      });
    });

    $('.context-menu a').click(function (e) {
      e.preventDefault();
      e.stopImmediatePropagation();

      window.scroll({
        top: ($($(this).attr('href')).offset().top - $('.context-menu').outerHeight() - 20), 
        left: 0, 
        behavior: 'smooth'
      });

    });

    $('.goup#goUp').click(function(){
      window.scrollTo({
        top: 0,
        behavior: 'smooth'
      });
    });

    $('.user-rate img').click(function () {
      var count = Number($(this).prevAll('img').length) + 1;
      $('.user-rate').removeClass(function (index, className) {
        return (className.match(/(^|\s)active-\S+/g) || []).join(' ');
      });
      $('.user-rate').addClass('active-' + count).attr('data-rating', count);
      $('#dodajOpinie input[name="rate"]').val(count);
    }
    );

    $('.scrollToReview').click(function () {
      $('html, body').animate({
        scrollTop: ($('#dodajOpinie').offset().top - $('.context-menu').outerHeight() - 20)
      }, 500);
    })

    //scroll

    if ($('.context-list').length > 0) {
      $(window).scroll(function () {
        $('.context-list li.active').removeClass('active');
        for (var i = 0; i < headingsHeight.length; i++) {
          if ($(window).scrollTop() >= headingsHeight[i].offtop - ($(window).height() * .55)) {
            for (var x = 0; x < i; x++) {
              $('.context-list li .' + headingsHeight[x].class).parents('li').removeClass('active');
            }
            $('.context-list li .' + headingsHeight[i].class).parents('li').addClass('active');
            if($('body').hasClass('postid-4430')){
              var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + '#' +headingsHeight[i].class;
              window.history.pushState({ path: newurl }, '', newurl);
            }
          }
        }
      });
    }

    $(document).scroll(function () {
      if ($(window).scrollTop() > ($(window).height() / 2)) {
        $('#goUp').addClass('visible');
      }
      else {
        $('#goUp').removeClass('visible');
      }
    })
    $(document).scroll(function () {
      if ($(window).scrollTop() > $('.title-menu').attr('data-offset-top')) {
        $('.title-menu').addClass('fixed');
      }
      else {
        $('.title-menu').removeClass('fixed');
      }
    })

    //hover
    $('.user-rate img').hover(function () {
      $(this).css('filter', 'grayscale(0)').prevAll().css('filter', 'grayscale(0)');
    },
      function () {
        $(this).parents('.user-rate').find('img').css('filter', 'grayscale(100%)');
      }
    );

    var timeouts;
    $('.posts-mosaic.poradniki .single').hover(
      function () {
        if($(window).width() < 900){
          return false;
        }
        var that = this;
        timeouts = setTimeout(function () {
          $(that).find('>.image').animate({
            height: "60px"
          }, 300);
          $(that).find('.excerpt').slideDown(300);
          $(that).find('.btn-holder').slideDown(300);
        }, 400);
      },
      function () {
        clearTimeout(timeouts);
        $(this).find('>.image').animate({
          height: $(this).find('>.image img').height()
        }, 300);
        $(this).find('.excerpt').slideUp(300);
        $(this).find('.btn-holder').slideUp(300)
      }
    );

    //resize

    $(window).resize(function () {
      initSlickOnTypes();
    });



    //functions


    function setCookie(name, value, days) {
      var expires = "";
      if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toUTCString();
      }
      document.cookie = name + "=" + (value || "") + expires + "; path=/";
    }

    function getCookie(name) {
      var nameEQ = name + "=";
      var ca = document.cookie.split(';');
      for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
      }
      return ' ';
    }

    function initSlickOnTypes() {
      if ($(window).width() < 900) {
        $('.slick-mobile:not(.slick-initialized)').slick({
          dots: true,
        });

        $('.page-template-tpl-news-lista .buks-boxes').slick({
          dots:true,
          slidesToShow:2,
          responsive: [
            {
              breakpoint: 660,
              settings: {
                slidesToShow: 1,
              }
            }
          ]
        });
      }
      else {
        $('.slick-mobile.slick-initialized').slick('unslick');
        $('.page-template-tpl-news-lista .buks-boxes.slick-initialized').slick('unslick');
      }
    };

  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
  },
};
