<?php
/*
Template Name: Typy single
Template Post Type: post, page
*/

$context = Timber::get_context();
$context['post'] = Timber::get_post();

$args = [
  'numberposts' => 3,
  'category' => array('49'),
  'post__not_in' => array($context['post']->ID)
];

$context['typy'] = Timber::get_posts($args);
$context['typy_link'] = get_category_link('49');

foreach($context['options']['kafelki_bukmacherzy'] as $single){
  $context['kafelki_bukmacherzy'][] = Timber::get_post($single['bukmacher']->ID);
}


Timber::render('views/templates/typy-single.twig', $context);
