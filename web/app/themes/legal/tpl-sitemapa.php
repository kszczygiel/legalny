<?php
/*
Template Name: Sitemapa
Template Post Type: post, page
*/

generate_array_excluded_from_csv();

$context = Timber::get_context();
$context['post'] = Timber::get_post();
$args = [
  'numberposts' => -1,
  'category' => array('283')
];

$context['recent'] = Timber::get_posts($args);

$args = [
  'numberposts' => -1,
  'category' => array('49')
];

$context['typy'] = Timber::get_posts($args);

$args = [
  'numberposts' => -1,
  'category' => array('112'),
  'meta_key'		=> 'wyroznione',
  'meta_value'	=> '0',
];

$context['poradniki'] = Timber::get_posts($args);

Timber::render('views/templates/sitemap.twig', $context);
