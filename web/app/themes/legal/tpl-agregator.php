<?php
/*
Template Name: Agregator kodów
Template Post Type: post, page
*/

$context = Timber::get_context();
$context['post'] = Timber::get_post();
$args = [
  'numberposts' => 2,
  'category' => array('283')
];

$context['recent'] = Timber::get_posts($args);
$context['recent_link'] = get_category_link('283');

Timber::render('views/templates/agregator.twig', $context);
