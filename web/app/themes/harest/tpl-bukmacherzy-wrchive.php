<?php
/*
Template Name: Bukmacherzy - archiwum
*/
get_header(); ?>

<?php 

$args = array(
	'posts_per_page' => -1,
	'numberposts' => -1,
	'post_type' => 'bookmaker'
);
?>

<div class="wpspw-post-grid-main wpspw-design-17 wpspw-image-fit wpspw-grid-3 wpspw-clearfix" style="    display: flex;
    flex-flow: row wrap;">
	<?php foreach(get_posts($args) as $single):?>
  <div class="wpspw-post-grid  wpspw-medium-4 wpspw-columns ">
    <div class="wpspw-post-grid-content">

			<div class="wpspw-post-title-content" style="position:initial; background:none;">
					<h2 class="wpspw-post-title"> <a
					style=" color:black; "
              href="<?php the_permalink($single) ?>"
              target="_self"><?php echo $single->post_title?></a>
          </h2>

        </div>
			<div class="wpspw-post-content">
        <div class="wpspw-post-sub-short-content"><?php echo wp_trim_words( $single->post_content, 20) ?></div> <a
          href="<?php the_permalink($single) ?>"
          target="_self" class="readmorebtn">Czytaj dalej</a>
      </div>
    </div>
	</div>
	<?php endforeach; ?>
</div>

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', 'page' ); ?>

			<?php endwhile; ?>

<?php get_footer(); ?>