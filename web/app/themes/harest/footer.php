<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id="main-core".
 *
 * @package ThinkUpThemes
 */
?>

		</div><!-- #main-core -->
		</div><!-- #main -->
		<?php /* Sidebar */ thinkup_sidebar_html(); ?>
	</div>
	</div><!-- #content -->
<?php /*  Pre-Designed HomePage Content */ thinkup_input_homepagesection(); ?>
	<footer>
<?php if($_GET['dev'] == 1):?>
	<div class="menus">
		<div class="row">
		<div class="col-md-3 col-sm-6">
			<h3>Bukmacherzy</h3>
			<ul>
			<?php foreach(wp_get_nav_menu_items('footer1') as $item):  ?>
				<li>
					<a href="<?php echo $item->url ?>"><?php echo $item->title ?></a>
				</li>
			<?php endforeach;?>

			</ul>

		</div>

		<div class="col-md-3 col-sm-6">
			<h3>Kody promocyjne</h3>
			<ul>
			<?php foreach(wp_get_nav_menu_items('footer2') as $item):  ?>
				<li>
					<a href="<?php echo $item->url ?>"><?php echo $item->title ?></a>
				</li>
			<?php endforeach;?>

			</ul>

		</div>
		<div class="col-md-3 col-sm-6">
			<h3>Najciekawsze</h3>
			<ul>
			<?php foreach(wp_get_nav_menu_items('footer3') as $item):  ?>
				<li>
					<a href="<?php echo $item->url ?>"><?php echo $item->title ?></a>
				</li>
			<?php endforeach;?>

			</ul>

		</div>
		<div class="col-md-3 col-sm-6">


		</div>
		
		</div>
	</div>

	<?php endif ?>

		<?php /* Custom Footer Layout */ thinkup_input_footerlayout();
		
		echo	'<!-- #footer -->';  ?>

		
		<div id="sub-footer">
		<div id="sub-footer-core">

			<div class="copyright">
                Developed by Think Up Themes Ltd. (thinkupthemes.com)
			<?php /* === Add custom footer === */ //thinkup_input_copyright(); ?>
			</div>
			<!-- .copyright -->

			<?php if ( has_nav_menu( 'sub_footer_menu' ) ) : ?>
			<?php wp_nav_menu( array( 'depth' => 1, 'container_class' => 'sub-footer-links', 'container_id' => 'footer-menu', 'theme_location' => 'sub_footer_menu' ) ); ?>
			<?php endif; ?>
			<!-- #footer-menu -->

			<?php if ( ! has_nav_menu( 'sub_footer_menu' ) ) : ?>
			<?php /* Social Media Icons */ thinkup_input_socialmediafooter(); ?>
			<?php endif; ?>

		</div>
		</div>
	</footer><!-- footer -->
        
     <center>   
  <img src="/img/18plus.png">  Hazard może uzależniać. Fortuna, STS, Forbet, Totolotek i Milenium to legalni bukmacherzy. Gra u nielegalnych grozi konsekwencjami prawnymi <img src="/img/stop.png">        
       </center>
 
        
</div><!-- #body-core -->

<?php wp_footer(); ?>

<script type="text/javascript" src="/wp-content/marquee.js"></script>


		<script>
			$(function (){

				$('.simple-marquee-container').SimpleMarquee();
				
			});

		</script>


  <script src="/boot/js/bootstrap.min.js" async></script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-98717213-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-98717213-2');
</script>
   <link rel="stylesheet" href="/wp-content/themes/harest/bootstrap.min.css"> 
    <link rel="stylesheet" href="/wp-content/themes/harest/oddscss.css">
   <link rel="stylesheet" href="/wp-content/themes/harest/menu/styles.css">


<!-- Adform Tracking Code BEGIN -->
<script type="text/javascript">
    window._adftrack = Array.isArray(window._adftrack) ? window._adftrack : (window._adftrack ? [window._adftrack] : []);
    window._adftrack.push({
        pm: 1672235,
        divider: encodeURIComponent('|'),
        pagename: encodeURIComponent('Affiliate site PL - Bukmacher-legalny.pl')
    });
    (function () { var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = 'https://a1.adform.net/serving/scripts/trackpoint/async/'; var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x); })();

</script>
<noscript>
    <p style="margin:0;padding:0;border:0;">
        <img src="https://a1.adform.net/Serving/TrackPoint/?pm=1672235&ADFPageName=Affiliate%20site%20PL%20-%20Bukmacher-legalny.pl&ADFdivider=|" width="1" height="1" alt="" />
    </p>
</noscript>
<!-- Adform Tracking Code END -->


</body>
</html>