<?php
/**
 * The Template for displaying all single posts.
 *
 * @package ThinkUpThemes
 */

get_header(); ?>

			<?php while ( have_posts() ) : the_post(); ?>
			
			

				<?php  get_template_part( 'content', 'single' ); ?>
				
				
<?php if(in_array($post->ID,array(3505,3538,182,3940,186,120,190,192,2702,109,2408,144))): ?>

<div class="row">


<?php if(count(get_csg_reviews($post->ID)) > 0):?>
        <section class="opinion opinion-csg col-md-9">
            <header><h2 id="csgReviewsHeader"><?php echo (get_field('h2_review_form') ? : 'Opinie');?></h2></header>
            <?php
            foreach (get_csg_reviews($post->ID) as $review):
                ?>
                <article class="single-item" style="justify-content: flex-start; align-items: center">
                    <div class="opinion-rating">
                        <div class="stars-holder">
                            <div class="credit-rate"
                                  data-rating="<?php the_field('ilosc_gwiazdek', $review->ID) ?>"></div>
                            <div class="rate-status">
                                <p><span>Ocena: </span><span class="amount"></span></p>
                            </div>
                        </div>
                    </div>
                    <div class="opinion-comment">
                    <p class="date"><span>Dodano: </span><span> <?php echo get_the_date('', $review->ID) ?></span></p>

                        <h4><?php the_field('name', $review->ID) ?></h4>
                        <div class="positive">
                          <p><?php the_field('pozytywna_opinia', $review->ID) ?></p>                        
                        </div>
                        <div class="negative">
                          <p><?php the_field('negatywna_opinia', $review->ID) ?></p>                        
                        </div>
                    </div>
                </article>
            <?php endforeach; ?>
        </section>
    <?php endif; ?>
  <div class="col-md-9" id="postReviewsForm">
  <?php if(!in_array($post->ID,explode(',',$_COOKIE['BlockReview']))):?>
  
    <div class="add-review" id="addReview">
        <header><h3>Dodaj opinię</h3></header>
        <form method="post" id="review">
            <div class="opinion-rating">
                <div class="stars-holder enable-click">
                    <div class="credit-rate user-rate" data-rating="5"></div>
                </div>
            </div>
            <input type="hidden" name="post_id" value="<?php echo $post->ID ?>">
            <input type="hidden" name="rate" value="5">
            <div class="inp-group">
                <label for="name">Imię*</label>
                <input type="text" name="name" required>
            </div>
            <div class="inp-group">
                <label for="message-positive">Co Ci się podobało</label>
                <textarea type="text" name="message-positive" required> </textarea>
            </div>
            <div class="inp-group">
                <label for="message-negative">Co Ci się NIE podobało</label>
                <textarea type="text" name="message-negative" required> </textarea>
            </div>
            <div class="btn-holder">
                <button type="button" class="btn">Zapisz</button>
            </div>
        </form>
    </div>
    <?php endif;?>
  </div>
</div>


<script>
  window.onload = function(){
    var list_number = parseInt($('#toc_container .toc_list>li').last().find('.toc_number').html()) + 1;
    $('#toc_container ul.toc_list').append('<li><a href="#csgReviewsHeader"><span class="toc_number toc_depth_1">'+ list_number +'</span> <?php echo (get_field('h2_review_form') ? : 'Opinie');?></a></li>');
  };
</script>

<?php endif; ?>				
<?php if(in_array('283',wp_get_post_categories($post->ID))): ?>
  <style>  #sidebar .wpspw_pro_post_list_widget{
  display:none;
  } 
  #underPostContentRelated .wpspw-pro-post-slider-widget{
  	    display: flex;
        flex-flow:row wrap;
        justify-content:space-between;
  }    #underPostContentRelated .wpspw-pro-post-slider-widget .wpspw-post-grid{
	width:calc((100% / 3) - 20px );
  }
  
  @media all and (max-width:640px){
     #underPostContentRelated .wpspw-pro-post-slider-widget .wpspw-post-grid{
	width:calc((100% / 2) - 20px );
  }
  }
    @media all and (max-width:480px){
     #underPostContentRelated .wpspw-pro-post-slider-widget .wpspw-post-grid{
	width:calc((100% / 1) );
  }
  }
  
  </style>
  <div class="row">
  	<div class="col-md-9" id="underPostContentRelated">
      <?php  dynamic_sidebar( 'under-single-post-content' ); ?>
    </div>
  </div>

<?php endif; ?>


				<?php wp_link_pages( array( 'before' => '<div class="page-links">' . __( 'Pages:', 'harest' ), 'after'  => '</div>', ) ); ?>

				<?php thinkup_input_nav( 'nav-below' ); ?>

				<?php edit_post_link( __( 'Edit', 'harest' ), '<span class="edit-link">', '</span>' ); ?>

				<?php thinkup_input_allowcomments(); ?>

			<?php endwhile; ?>

<script type="text/javascript">var ajaxurl = "<?php echo admin_url('admin-ajax.php') ?>";</script>


<?php get_footer(); ?>

