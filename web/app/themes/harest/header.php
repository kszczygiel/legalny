<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up until id="main-core".
 *
 * @package ThinkUpThemes
 */
?><!DOCTYPE html>

<html <?php language_attributes(); ?>>
<head>


<link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
<link rel="manifest" href="/manifest.json">
<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">




<?php thinkup_hook_header(); ?>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<link rel="profile" href="//gmpg.org/xfn/11" />
<link rel="pingback" href="<?php esc_url( bloginfo( 'pingback_url' ) ); ?>" />



<?php wp_head(); ?>
<!--
   <link rel="stylesheet" href="https://bukmacher-legalny.pl/wp-content/themes/harest/bootstrap.min.css"> 
    <link rel="stylesheet" href="https://bukmacher-legalny.pl/wp-content/themes/harest/oddscss.css">
   <link rel="stylesheet" href="https://bukmacher-legalny.pl/wp-content/themes/harest/menu/styles.css">
-->
    <!-- jQuery library -->
    <!-- Latest compiled JavaScript -->
 <!--   <script src="https://bukmacher-legalny.pl/boot/js/bootstrap.min.js" async></script>-->
   <script src="/wp-content/themes/harest/menu/script.js" async></script>
    
    


<style>
    @media all and (min-width: 601px) {
    #topmob{
      display:none;
    }
}
@media all and (max-width: 600px) {
    #topmob {
       padding:0px
    }
}
    
    
    </style>    
    


<script>
jQuery(document).ready(function(){

   jQuery("a").each(function(index) {
    var link = jQuery(this).attr('href');
    if(link.indexOf("/go/") >= 0) {jQuery(this).attr('rel', "nofollow noindex");
                                   }    
                              }); 


}); 

</script>

    
</head>

<body <?php body_class(); ?>>




<div id="body-core" class="hfeed site">



	<header>
        

        <!-- <div id="topmob"><img src="/img/mobile-top_c_211_134.png"></div> -->



	<div id="site-header">
<!--        
      <div id='cssmenu'>
<ul>
   <li><a href='/'>Legalny bukmacher</a></li>


    <li><a href='/blog'>Blog</a></li>
   <li><a href='/abc-legalnosci/' title="abc legalnosci">ABC Legalności</a></li>
    <li><a href='/legalni-bukmacherzy-internetowi-w-polsce/' title="legalni bukmacherzy">Legalni bukmacherzy</a></li> 
   <li><a href='/poradnik/' title="poradnik bukmacherski">Poradnik</a></li>  
    <li><a href='/mecze-na-zywo/' title="mecze na żywo">Mecze na żywo</a></li> 
    
   <li class='has-sub'><a href='#'>Bukmacherzy</a>
      <ul>
         <li><a href='/fortuna-zaklady-bukmacherskie/' title="Fortuna"><span>Fortuna</span></a></li>
        <li><a href='/lvbet-zaklady-bukmacherskie/' title="LVBET"><span>LVBET</span></a></li>
           <li><a href=' /forbet-zaklady-bukmacherskie/' title="Forbet"><span>Forbet</span></a></li>
           <li><a href=' /pzbuk-zaklady-bukmacherskie/' title="PZBUK"><span>PZBUK</span></a></li>
           
           <li><a href=' /totolotek-zaklady-bukmacherskie/' title="Totolotek"><span>Totolotek</span></a></li>
           <li><a href=' /milenium-zaklady-bukmacherskie/' title="Milenium"><span>Milenium</span></a></li>
           <li><a href=' /totalbet-zaklady-bukmacherskie-recenzja/' title="Totalbet"><span>Totalbet</span></a></li>
           <li><a href=' /sts-zaklady-bukmacherskie/' title="STS"><span>STS</span></a></li>
           <li class='last'><a href=' /bukmacher-etoto-oferta/' title="Etoto"><span>Etoto</span></a></li>
      </ul>
   </li>
    
   <li><a href=' /typy-na-dzis/' title="typy na dziś">Typy na dziś</a></li>


</ul>
</div>   
        -->
        

        
        
	<div id="pre-header" style="padding:0px">
		<div class="wrap-safari" style="height:auto">
		<div id="pre-header-core" class="main-navigation">
            

			<?php if ( has_nav_menu( 'pre_header_menu' ) ) : ?>
            
             
            
			<?php  wp_nav_menu( array( 'container_class' => 'header-links', 'container_id' => 'pre-header-links-inner', 'theme_location' => 'pre_header_menu' ) ); ?>
			<?php endif; ?>

			<?php /* Social Media Icons */ //thinkup_input_socialmediaheaderpre(); ?>

		</div>

		</div>
		</div>

<?php
            $term = get_queried_object();
            $header_h2 = ( is_category() ? get_field('header_naglowek_h2', $term) : get_field('header_naglowek_h2') );
            $header_h3 = ( is_category() ? get_field('header_naglowek_h3', $term) : get_field('header_naglowek_h3') );

?>

 <div class="jumbotron text-center" id="desktop"> 
<div class="container" style="height:200px">

  <?php if ( is_home() || is_front_page() ) : ?>
     <h2>
     <span class="label-danger" style="background: rgba(51, 122, 183, 0.5); padding: 10px;     border-radius: 5px;     color: white; font-size:63px;">
      <?php echo (get_field('header_naglowek_h2') ? : 'Legalni Bukmacherzy - Bukmacher 2020'); ?>
     </span>
     </h2>
	<?php endif; ?>
  <?php if ( !is_home() && ! is_front_page() ) : ?>
	<h2><span class="label-danger" style="background: rgba(51, 122, 183, 0.5); padding: 10px;  font-size:50px;   border-radius: 5px;      line-height: 50px;   color: white;">
   <?php echo ($header_h2 ? : 'Legalni bukmacherzy 2020'); ?></span></h2></br>
	<?php endif; ?>
        <h3 class="label label-info" style="font-size:21px; font-weight:normal;"><?php echo ($header_h3 ? : 'Wybierz najlepszą ofertę na rynku'); ?></h3>
    </div>
</div>


		<?php if ( get_header_image() ) : ?>
			<div class="custom-header"><img src="<?php header_image(); ?>" width="<?php echo esc_attr( get_custom_header()->width ); ?>" height="<?php echo esc_attr( get_custom_header()->height ); ?>" alt=""></div>
		<?php endif; // End header image check. ?>

	
		<!-- #pre-header 

		<div id="header">
		<div id="header-core">

			<div id="logo">
			
			</div>

			<div id="header-links" class="main-navigation">
                
               
                
			<div id="header-links-inner" class="header-links">

				<?php  //$walker = new thinkup_menudescription;
				//wp_nav_menu(array( 'container' => false, 'theme_location'  => 'header_menu', 'walker' => new thinkup_menudescription() ) ); ?>
				
				<?php /* Header Search */ //thinkup_input_headersearch(); ?>
			</div>
			</div>
			<!-- #header-links .main-navigation 

			<?php /* Add responsive header menu */ // thinkup_input_responsivehtml1(); ?>


		</div>
		</div> -->
		<!-- #header -->

		<?php /* Add responsive header menu */ // thinkup_input_responsivehtml2(); ?>

	</div>
<!-- BOKSY START -->
<?php  include ("./boksy-nowy.php"); ?><br>
<!-- BOKSY END -->



	<?php /* Custom Slider */ thinkup_input_sliderhome(); ?>

	</header>
	<!-- header -->
	<?php /*  Call To Action - Intro */ thinkup_input_ctaintro(); ?>

	<?php /* Custom Intro */ thinkup_custom_intro(); ?>
	

	<div id="content" style="padding-top:5px">
	<div id="content-core">

		<div id="main">
		<div id="main-core">