﻿<?php
/*
Template Name: LIVE
*/

get_header(); ?>


<div class="container">
    
    <div class="row">
    
        <div class="col-md-3"><a href="https://bukmacher-legalny.pl/mecze-na-zywo-pilka-nozna/" class="btn btn-info btn-block" role="button"><img src="/img/pilkanozna.png" width="25px"> Piłka nożna</a></div>
<div class="col-md-3"><a href="https://bukmacher-legalny.pl/mecze-na-zywo-koszykowka/" class="btn btn-info btn-block" role="button"><img src="/img/koszykowka.png" width="25px"> Koszykówka</a></div>
<div class="col-md-3"><a href="https://bukmacher-legalny.pl/mecze-na-zywo-siatkowka/" class="btn btn-info btn-block" role="button"><img src="/img/siatkowka.png" width="25px"> Siatkówka</a></div>
<div class="col-md-3"><a href="https://bukmacher-legalny.pl/mecze-na-zywo-pilka-reczna/" class="btn btn-info btn-block" role="button"><img src="/img/pilkareczna.png" width="25px"> Piłka ręczna</a></div>
</div>
<br>

  <div class="row">

<div class="col-md-3"><a href="https://bukmacher-legalny.pl/mecze-na-zywo-hokej/" class="btn btn-info btn-block" role="button"><img src="/img/hokej.png" width="25px"> Hokej</a></div>
<div class="col-md-3"><a href="https://bukmacher-legalny.pl/mecze-na-zywo-tenis/" class="btn btn-info btn-block" role="button"><img src="/img/tenis.png" width="25px"> Tenis</a></div>
<div class="col-md-3"><a href="https://bukmacher-legalny.pl/mecze-na-zywo/" class="btn btn-success btn-block" role="button">Wszystko</a></div>
    
</div>
    </div>
    <div class="row">
     <div class="col-md-12" style="background-color:white;  border-radius:5px; padding:5px;">
    </div>

                <div class="row" >    
                    
                    
                    <div class="col-md-12" style="background-color:white">   
               
                        <a name="mecz"></a>


                        <?php $tracker=$_GET['id']; ?>
                        <?php the_content(); ?>
                        <a name="mecz"></a>

                       <? echo $tracker; ?>

                            <section id="tracker-1" class="STATSCORE__Tracker" data-event="<?php echo $tracker; ?>" data-lang="pl" data-config="278" data-zone="">
                            </section>

                    </div>

                </div>
            </div>


<?php get_footer(); ?>

<script rel="script" src="https://live.statscore.com/livescorepro/generator"></script>