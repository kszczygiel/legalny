<?php
/**
 * The Single Post content template file.
 *
 * @package ThinkUpThemes
 */
?>

		<?php thinkup_input_postmedia(); ?>
		<?php thinkup_input_postmeta(); ?>

		<div class="row">

		

			<div class="col-md-9">

										<div class="entry-content" style="text-align:justify">
								<?php

								//echo "<div class='image-full img img-responsive' style='width:100%;'><center>"; the_post_thumbnail('full'); echo "</center></div>"; ?>

								<div class='image-full img img-responsive' style='width:100%;'><center>
                  <?php if(get_field('featured_image_url')):?>
                    <a href="<?php the_field('featured_image_url') ?>">
                <img src="<?php the_post_thumbnail_url(); ?>" style="max-height: 500px;"/>
                  </a>
                  <?php else:?>
                    <img src="<?php the_post_thumbnail_url(); ?>" style="max-height: 500px;"/>

                  <?php endif; ?>
								</center></div>




								<?php $home = get_field( "home_team");  
								      $away = get_field( "away_team"); ?>

								  
								<?php if( get_field('home_team') &&  get_field('away_team')): ?>
								    

								    
								<!--     

								<div class="container-fluid">
								<div class="row" style="padding-bottom: 20px;">
								<div class="col-md-4 col-xs-4" style="text-align: center;">
								    <img class="img-responsive" style="max-height: 110px; width: auto; margin:0 auto;" src="<? //echo $home; ?>" alt="" width="110" height="110" />
								</div>
								    
								<div class="col-md-4 col-xs-4" style="text-align: center;">
								    <img class="img-responsive" style="max-height: 110px; width: auto; margin:0 auto;"  src="/kluby/vs.jpg" alt="" width="110" height="110" />
								</div>
								    
								<div class="col-md-4 col-xs-4" style="text-align: center;">
								    <img class="img-responsive" style="max-height: 110px; width: auto; margin:0 auto;" src="<? //echo $away; ?>" alt="" width="110" height="110" />
								</div>
								</div>
								</div> -->
								    
								    <?php endif; ?>    







											<?php the_content();  ?>
											
		
											
											
										</div><!-- .entry-content -->




</div>

	<div class="col-md-3">
			<?php get_sidebar(); ?>

			</div>



</div>




		<div class="clearboth"></div>