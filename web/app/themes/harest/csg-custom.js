var $ = jQuery;

$(document).ready(function () {
  $('#review button').click(function () {
    jQuery.ajax({
      url: ajaxurl,
      data: {
        action: 'ajax_save_review',
        data: $('#review').serialize()
      },
      type: 'POST',
      success: function (data) {
        var info = JSON.parse(data);
        new Noty({
          type: info.type,
          text: info.message,
          progressBar: true,
          theme: 'mint',
          timeout: 3000
        }).show();
        if (info.type == "success") {
          $('#addReview').remove();
          setCookie('BlockReview', getCookie('BlockReview') + ',' + info.post_id, 365);
        }
      }
    });
  });

  initStars();

  $('.user-rate').click(function(){
    var value = ( $('.user-rate .jq-ry-rated-group.jq-ry-group').width() / $('.jq-ry-normal-group.jq-ry-group').width() ) * 5;
    $('form#review').find('input[name="rate"]').val(value);
  }); 

  initSlickBuks();

  $(window).resize(function(){
    initSlickBuks();
  });



  function initSlickBuks(){
    if($(window).width() < 991){
      $('.slick-slider-buks').slick({
        infinite: true,
        dots:false,
        arrows:false,
        autoplay: true,
        autoplaySpeed: 3000,
      });
    }
    else if($('.slick-slider-buks').hasClass('slick-initialized')){
      $('.slick-slider-buks').slick('unslick');
    }
  }

  function setCookie(name, value, days) {
    var expires = "";
    if (days) {
      var date = new Date();
      date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
      expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "") + expires + "; path=/";
  }

  function initStars() {
    $(".credit-rate:not(.user-rate)").rateYo({
      rating: 0,
      halfStar: true,
      width: '85px',
    });
    $(".user-rate").rateYo({
      rating: 0,
      fullStar: true,
      width: '85px',
    });
    $(".credit-rate").each(function () {
      var rating = $(this).data("rating");
      $(this).rateYo("rating", rating);
      $(this).siblings('.rate-status').find('.amount').text(rating);
    });
  }


  function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') c = c.substring(1, c.length);
      if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return ' ';
  }

});