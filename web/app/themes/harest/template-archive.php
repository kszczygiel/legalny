<?php
/**
 * Template Name: Archive
 *
 * @package ThinkUpThemes
 */

get_header(); ?>

			<div class="one_half">	
				<h4 class="page-title"><?php _e( 'Pages', 'harest' ); ?>:</h4>
				<ul class="archive-pages">
					<?php wp_list_pages('title_li='); ?>
				</ul>
			</div>

			<div class="one_half last">
				<h4 class="page-title"><?php _e( 'Recent Posts', 'harest' ); ?>:</h4>
				<ul class="archive-recent">
					<?php 	$recent_posts = wp_get_recent_posts();
							foreach( $recent_posts as $recent ){
							echo '<li><a href="' . esc_url( get_permalink($recent["ID"]) ) . '">' . esc_html( $recent["post_title"] ) . '</a></li>';
							} 
					?>
				</ul>

				<h4 class="page-title"><?php _e( 'Monthly', 'harest' ); ?>:</h4>
				<ul class="archive-monthly">
					<?php wp_get_archives( 'show_post_count=1' ); ?>
				</ul>

				<h4 class="page-title"><?php _e( 'Categories', 'harest' ); ?>:</h4>
				<ul class="archive-categories">
					<?php wp_list_categories( 'show_count=1&title_li=' ); ?>
				</ul>

				<h4 class="page-title"><?php _e( 'Author(s)', 'harest' ); ?>:</h4>
				<ul class="archive-authors">
					<?php wp_list_authors( 'optioncount=1' ); ?>
				</ul>
			</div><div class="clearboth"></div>

<?php get_footer(); ?>  