<?php
/**
 * The template for displaying Archive pages.
 *
 * @package ThinkUpThemes
 */

get_header(); ?>

<?php $category = get_queried_object();
$kat= $category->term_id;
//echo $kat;

?>

<?php
$the_query = new WP_Query( array( 'tag_id' => $kat ) );

if ( $the_query->have_posts() ) {
    echo '<div class="container-fluid">';
    while ( $the_query->have_posts() ) {
        $the_query->the_post();

        echo '<div class="row">';
        echo '<div class="col-md-12"><h4><a href="'. get_permalink(). '">' . get_the_title() . '</a></h4></div>';
        echo '</div>';

        echo '<div class="row">';

        echo '<div class="col-md-3" >';
        the_post_thumbnail('thumbnail');
        echo '</div>';        
        echo '<div class="col-md-9">'. the_excerpt() . '</div>';
        echo '</div>';
    }
    echo '</div>';
} else {
    // no posts found
}
/* Restore original Post Data */
wp_reset_postdata();

?>

<?php 

//if($kat==112 || $kat==275 || $kat==49)
{

$kategoria=" [wpspw_post limit='12' design='design-17' grid='3' category='".$kat."' pagination='true' pagination_type='numeric' read_more_text='Czytaj dalej' show_tags='false' show_author='false' content_words_limit='30' ] ";

echo do_shortcode($kategoria); 
//echo $kat;



}

 echo category_description($kat); 
/*
if($kat!=275 || $kat!=49)
    
            {

            $tag = get_tag($kat); // <-- your tag ID
            echo $tag->name;

            $the_query = new WP_Query( 'tag='.$tag->name );

            if ( $the_query->have_posts() ) {
                echo '<ul>';
                while ( $the_query->have_posts() ) {
                    $the_query->the_post();
                    
                    ?>

<a href="<?php the_permalink(); ?>">
<?
                    
                    echo '<li>' . get_the_title() . '</li></a>';
                }
                echo '</ul>';
            } else {
                // no posts found 
            }
            /* Restore original Post Data */
           // wp_reset_postdata();
           // }
//else {
 
?> 





				


			
<?php get_footer() ?>